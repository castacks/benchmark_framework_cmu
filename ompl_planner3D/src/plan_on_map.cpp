#include <ompl_planner3D/plan_on_map.h>

namespace ompl_planner_3D {

OMPLPlanner3D::OMPLPlanner3D()
    : initialized_(false), tree(NULL){}

OMPLPlanner3D::OMPLPlanner3D(std::string name,tf::TransformListener& tf)
    : initialized_(false), tree(NULL)
{

    ROS_INFO("STARTING THE PLANNER");
    initialize(name);
}

OMPLPlanner3D::~OMPLPlanner3D()
{
}


void OMPLPlanner3D::initialize(std::string name)
{
    if(!initialized_)
    {
        //create Nodehandle
        ros::NodeHandle private_nh("~/");
        private_nh_ = private_nh;

        tf_ =  new tf::TransformListener(ros::Duration(5));
        ROS_INFO("INTIALIAZING");

        goalSet =false;

        // get parameters from prms-server for planner (robot-geometry + environment are obtained from coastmap)
        private_nh_.param("max_dist_between_pathframes", max_dist_between_pathframes_, 0.10);
        private_nh_.param("max_footprint_cost", max_footprint_cost_, 256);
        private_nh_.param("relative_validity_check_resolution", relative_validity_check_resolution_, 0.004);
        private_nh_.param("interpolate_path", interpolate_path_, false);
        private_nh_.param("publish_diagnostics", publish_diagnostics_, false);
        timer1 = private_nh_.createTimer(ros::Duration(1.0), &OMPLPlanner3D::pathValidator,this);
        // paramter for planner type is read in makeplan routine --> allow resetting planner without reinitializing plugin

        // advertise topics
        plan_pub_ = private_nh_.advertise<nav_msgs::Path>("plan", 1);
        if(publish_diagnostics_)
        {
            diagnostic_ompl_pub_ = private_nh_.advertise<ompl_ros_interface::OmplPlannerDiagnostics>("diagnostics_ompl", 1);
            stats_ompl_pub_ = private_nh_.advertise<ompl_planner3D::OMPLPlannerBaseStats>("statistics_ompl", 1);
        }

        std::string octo_topic,goal_topic;
        private_nh_.param("octomap_topic", octo_topic, std::string("/octomap_full"));
        private_nh_.param("goal_topic", goal_topic, std::string("/move_base_simple/goal"));
        octomap_subs_ = private_nh_.subscribe(octo_topic,1,&OMPLPlanner3D::octomap_process,this);
        goal_subs_ = private_nh_.subscribe(goal_topic,1,&OMPLPlanner3D::goal_process,this);
        pcdPub = private_nh_.advertise<sensor_msgs::PointCloud>("/checkedStatesPointCloud1",1);
        pcdPub2 = private_nh_.advertise<sensor_msgs::PointCloud>("/checkedStatesPointCloud2",1);
        pcd.header.frame_id="/nav";

        // check whether parameters have been set to valid values
        if(max_dist_between_pathframes_ <= 0.0)
        {
            ROS_WARN("Assigned Distance for interpolation of path-frames invalid. Distance must be greater to 0. Distance set to default value: 0.10");
            max_dist_between_pathframes_ = 0.10;
        }

        initialized_ = true;
        ROS_INFO("ININT DONE");
    }
    else
        ROS_WARN("This planner has already been initialized... doing nothing");
}

bool doOnce = true;
void OMPLPlanner3D::octomap_process(const octomap_msgs::OctomapBinary::ConstPtr& msg)
{
//    ROS_INFO("RECVD OCTOMAP");
    //    tree = dynamic_cast<octomap::OcTree*>(octomap_msgs::binaryMsgDataToMap(msg->data));

    boost::mutex::scoped_lock lock(OctomapMutex);
    delete tree;
    if(tree)
    {
        tree = NULL;
    }
    if(!tree)
    {
        tree = dynamic_cast<octomap::OcTree*>(octomap_msgs::fullMsgDataToMap(msg->data));
    }
    if(tree && doOnce)
    {
        doOnce = false;
//        tree->setOccupancyThres(0.5);
//        tree->setProbHit(0.7);
//        tree->setProbMiss(0.3);
//        tree->setClampingThresMax(0.95);
//        tree->setClampingThresMin(0.1);
    }
}

void OMPLPlanner3D::goal_process(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    goalSet = true;
    geometry_msgs::PoseStamped start_g;
    if(!getCurrentPose(&start_g))
        ROS_ERROR("BAD CURRENT POSE");
    goal_ = *msg;
    if(goal_.pose.position.z < 0.5)
        goal_.pose.position.z = 10.0;

    {
        boost::mutex::scoped_lock lock(OctomapMutex);
        octomap::OcTreeNode* n;
        if(tree)
        {
            ROS_ERROR("TREE IS GOOD");
            n = tree->search(goal_.pose.position.x,goal_.pose.position.y,goal_.pose.position.z);
            //        if(n)
            //            ROS_ERROR("GOAL VALUE = %f",(float)n->getValue());
            if(!n)
                ROS_ERROR("BAD NODE");
        }
        else
            ROS_ERROR("BAD TREE");
    }
    makePlan(start_g,goal_,plan_);
//    if(pcd.points.size()>0){
//        pcdPub.publish(pcd);
//        pcd.points.clear();
//    }

//    delete tree;
//    if(tree)
//    {
//        tree = NULL;
//    }
}

bool OMPLPlanner3D::getCurrentPose(geometry_msgs::PoseStamped* start_g)
{
    geometry_msgs::PoseStamped start;
    start.header.frame_id="/base_link";
    start.header.stamp = ros::Time::now();
    start.pose.position.x = start.pose.position.y = start.pose.position.z = 0.0;
    //    start.pose.position.z = 1.0;
    start.pose.orientation.x =start.pose.orientation.y =start.pose.orientation.z = 0.0;
    start.pose.orientation.w = 1;

    //    tf_->waitForTransform("/nav",ros::Time(0),start.header.frame_id,ros::Time(0),"/nav",ros::Duration(5));
    ros::Time now = ros::Time::now();
    tf_->waitForTransform("/nav",start.header.frame_id,now,ros::Duration(5.0));
    try{
        tf_->transformPose("/nav",start,*start_g);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        return false;
    }
    return true;
}

bool OMPLPlanner3D::makePlan(const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal, std::vector<geometry_msgs::PoseStamped>& plan)
{

    if(!initialized_)
    {
        ROS_ERROR("The planner has not been initialized, please call initialize() to use the planner");
        return false;
    }

    ROS_DEBUG("Got a start: %.2f, %.2f, and a goal: %.2f, %.2f", start.pose.position.x, start.pose.position.y, goal.pose.position.x, goal.pose.position.y);

    // clear path
    plan.clear();

    // make sure goal is set in the same frame, in which the map is set
    if(goal.header.frame_id != "/nav"){
        ROS_ERROR("This planner as configured will only accept goals in the %s frame, but a goal was sent in the %s frame.","/nav",goal.header.frame_id.c_str());
        return false;
    }

    // instantiate variables for statistics and diagnostics plotting
    ros::Time start_time, end_time;
    // init msg for publishing of stats
    ompl_planner3D::OMPLPlannerBaseStats msg_stats_ompl;
    // init msg for publishing of diagnostics
    ompl_ros_interface::OmplPlannerDiagnostics msg_diag_ompl;
    // set start time for logging of planner statistics
    if(publish_diagnostics_)
        start_time = ros::Time::now();

    // everything alright -> now init the ompl planner
    // create inctance of the manifold to plan in -> for mobile base SE2
    ompl::base::StateSpacePtr manifold(new ompl::base::SE3StateSpace());

    // set bounds for the planner
    // as goal and map are set in same frame (checked above) we can directly get the extensions of the manifold from the map-prms
    ompl::base::RealVectorBounds bounds(3);
    double map_upperbound, map_lowerbound;

    // get bounds for x coordinate
    //	map_upperbound = costmap_.getSizeInMetersX() - costmap_.getOriginX();
    //	map_lowerbound = map_upperbound - costmap_.getSizeInMetersX();
    float diff_X = fabs(start.pose.position.x - goal.pose.position.x);
    float diff_Y = fabs(start.pose.position.y - goal.pose.position.y);

    float boundLimit = (diff_X >= diff_Y) ? (diff_X+20.0) : (diff_Y+20.0);
    map_upperbound = start.pose.position.x + boundLimit;//costmap_.getSizeInMetersX() + costmap_.getOriginX();
    map_lowerbound = start.pose.position.x - boundLimit;//map_upperbound - costmap_.getSizeInMetersX();
    bounds.setHigh(0, map_upperbound);
    bounds.setLow(0, map_lowerbound);
    ROS_DEBUG("Setting uper bound and lower bound of map x-coordinate to (%f, %f).", map_upperbound, map_lowerbound);

    // get bounds for y coordinate
    //	map_upperbound = costmap_.getSizeInMetersY() - costmap_.getOriginY();
    //	map_lowerbound = map_upperbound - costmap_.getSizeInMetersY();
    map_upperbound = start.pose.position.y + boundLimit;//costmap_.getSizeInMetersY() + costmap_.getOriginY();
    map_lowerbound = start.pose.position.y - boundLimit;//map_upperbound - costmap_.getSizeInMetersY();
    bounds.setHigh(1, map_upperbound);
    bounds.setLow(1, map_lowerbound);
    ROS_DEBUG("Setting uper bound and lower bound of map y-coordinate to (%f, %f).", map_upperbound, map_lowerbound);

    // setting bounds for Z coordinate
    private_nh_.param("z_upperbound", map_upperbound, 80.0);
    private_nh_.param("z_lowerbound", map_lowerbound, 0.0);
//    map_upperbound = 80;//start.pose.position.z + 15.0;
//    map_lowerbound = 0.0;//start.pose.position.z - boundLimit;//0.0;
    bounds.setHigh(2, map_upperbound);
    bounds.setLow(2, map_lowerbound);
    ROS_DEBUG("Setting uper bound and lower bound of map y-coordinate to (%f, %f).", map_upperbound, map_lowerbound);

    // now set it to the planer
    manifold->as<ompl::base::SE3StateSpace>()->setBounds(bounds);

    // now create instance to ompl setup
    ompl::geometric::SimpleSetup simple_setup(manifold);


    // set state validity checker
    simple_setup.setStateValidityChecker(boost::bind(&OMPLPlanner3D::isStateValid3DGrid, this, _1));
    // this call deviates a little bit from the example, as we use the member function of _this_ instance (initialized with the correct map, ...)
    // boost::bind works the following way
    // boost::bind(Adress of Function to pass,
    //			   Adress of instance to which the function shall be associated, - only for member functions -
    //			   list of parameters to pass to the function)
    // a nice introduction is found on http://blog.orionedwards.com/2006/09/function-pointers-in-cc-and-boostbind.html


    // get SpaceInformationPointer from simple_setup (initialized in makePlan routine)
    ompl::base::SpaceInformationPtr si_ptr = simple_setup.getSpaceInformation();
    if(0)
        si_ptr->printSettings();

    // set validity checking resolution
    si_ptr->setStateValidityCheckingResolution(relative_validity_check_resolution_);


    // convert start and goal pose from ROS PoseStamped to ompl ScopedState for SE2
    // convert PoseStamped into Pose2D
    geometry_msgs::PoseStamped start_local;
    tf_->transformPose("/nav",start,start_local);
    geometry_msgs::Pose start3D, goal3D;
    PoseToPose3D(start_local.pose, start3D);
    PoseToPose3D(goal.pose, goal3D);

    // before starting planner -> check whether target configuration is free
    int sample_costs = footprintCost2(goal3D.position.x, goal3D.position.y, goal3D.position.z,5.0);

    if( (sample_costs < 0.0) || (sample_costs > max_footprint_cost_) )
    {
        ROS_ERROR("Collision on target: Planning aborted! Change target position.");
        return false;
    }
    // before starting planner -> check whether start configuration is free
    sample_costs = footprintCost2(goal3D.position.x, goal3D.position.y, goal3D.position.z,5.0);// THIS IS WRONG, RECTIFY THIS LATER GD, use start pose.
    if( (sample_costs < 0.0) || (sample_costs > max_footprint_cost_) )
    {
        ROS_ERROR("Collision on start: Planning aborted! Free start position.");
        return false;
    }
    //	if(publish_diagnostics_)
    //	{
    //		// set start and end pose, as well as distance between poses
    //		msg_stats_ompl.start = start.pose;
    //		msg_stats_ompl.goal = goal.pose;
    //		msg_stats_ompl.start_goal_dist = sqrt((goal2D.x - start2D.x)*(goal2D.x - start2D.x) + (goal2D.y - start2D.y)*(goal2D.y - start2D.y));
    //	}

    // convert Pose2D to ScopedState
    ROS_DEBUG("Converting Start (%f, %f, %f) and Goal State (%f, %f, %f) to ompl ScopedState format", start3D.position.x, start3D.position.y, start3D.position.z, goal3D.position.x, goal3D.position.y, goal3D.position.z);

    // create a Scoped State according to above specified Manifold (SE2)
    ompl::base::ScopedState<> ompl_scoped_state_start(manifold);

    // and set this state to the start pose
    ROSPose3DToOMPLScopedStateSE3(ompl_scoped_state_start, start3D);
    //    ROS_INFO("START POSITION");
    //    ompl_scoped_state_start.print();

    // check whether this satisfies the bounds of the manifold
    //    ompl_scoped_state_start.random();
    bool inBound = manifold->satisfiesBounds(ompl_scoped_state_start->as<ompl::base::SE3StateSpace::StateType>());
    if(!inBound)
    {
        ROS_ERROR("Start Pose lies outside the bounds of the map - Aborting Planer");
        return false;
    }

    // create a Scoped State according to above specified Manifold (SE2)
    ompl::base::ScopedState<> ompl_scoped_state_goal(manifold);

    // and set this state to goal pose
    ROSPose3DToOMPLScopedStateSE3(ompl_scoped_state_goal, goal3D);
    //    ROS_INFO("GOAL POSITION");
    //    ompl_scoped_state_goal.print();

    // check whether this satisfies the bounds of the manifold
    //    ompl_scoped_state_goal.random();
    inBound = manifold->satisfiesBounds(ompl_scoped_state_goal->as<ompl::base::SE3StateSpace::StateType>());
    if(!inBound)
    {
        ROS_ERROR("Target Pose lies outside the bounds of the map - Aborting Planer");
        return false;
    }


    // set start and goal state to planner
    simple_setup.setStartAndGoalStates(ompl_scoped_state_start, ompl_scoped_state_goal);

    //====================BENCHMARKING STARTS HERE====================================================
    if(0)
    {

        ompl::tools::Benchmark b(simple_setup,"GD_EXPERIMENT");
        b.addPlanner(ompl::base::PlannerPtr(new ompl::geometric::KPIECE1(simple_setup.getSpaceInformation())));
        b.addPlanner(ompl::base::PlannerPtr(new ompl::geometric::RRT(simple_setup.getSpaceInformation())));
        b.addPlanner(ompl::base::PlannerPtr(new ompl::geometric::SBL(simple_setup.getSpaceInformation())));
        b.addPlanner(ompl::base::PlannerPtr(new ompl::geometric::LBKPIECE1(simple_setup.getSpaceInformation())));

        ompl::tools::Benchmark::Request req;
        req.maxTime = 5.0;
        req.maxMem = 100.0;
        req.runCount = 50;
        req.displayProgress = true;
        b.benchmark(req);
        b.saveResultsToFile();
    }

    //====================BENCHMARKING STOPS HERE====================================================
    // read desired planner-type from parameter server and set according planner to SimpleSetup
    setPlannerType(simple_setup);

    // finally --> plan a path (give ompl 1 second to find a valid path)
    ROS_DEBUG("Requesting Plan");
    bool solved = simple_setup.solve(10.0);//1.0

    if(!solved)
    {
        ROS_WARN("No path found");

        if(publish_diagnostics_)
        {
            // but still publish diagnostics of ompl -> compose msg
            msg_diag_ompl.summary = "Planning Failed";
            msg_diag_ompl.group = "base";
            msg_diag_ompl.planner = planner_type_;
            msg_diag_ompl.result =  "failed";
            msg_diag_ompl.planning_time = simple_setup.getLastPlanComputationTime();
            msg_diag_ompl.trajectory_size = 0;
            msg_diag_ompl.trajectory_duration = 0.0; // does not apply
            //msg_diag_ompl.state_allocator_size = simple_setup.getPlanner()->getSpaceInformation()->getStateAllocator().size();
            // publish msg
            diagnostic_ompl_pub_.publish(msg_diag_ompl);
        }

        return false;
    }
    else
        ROS_INFO("SOLVED <<<<<<<<<<<<");


    if(publish_diagnostics_)
    {
        // prepare diagnostic msg -> we do that before simplifying the plan to make sure we get the right computation time
        msg_diag_ompl.summary = "Planning success";
        msg_diag_ompl.group = "base";
        msg_diag_ompl.planner = planner_type_;
        msg_diag_ompl.result = "success";
        msg_diag_ompl.planning_time = simple_setup.getLastPlanComputationTime();
    }

    // give ompl a chance to simplify the found solution
    simple_setup.simplifySolution();

    // if path found -> get resulting path
    ompl::geometric::PathGeometric ompl_path(simple_setup.getSolutionPath());


    if(publish_diagnostics_)
    {
        // finish composition of msg
        msg_diag_ompl.trajectory_size = ompl_path.getStates().size();
        msg_diag_ompl.trajectory_duration = 0.0; // does not apply
        //msg_diag_ompl.state_allocator_size = simple_setup.getPlanner()->getSpaceInformation()->getStateAllocator().size();
        // publish msg
        diagnostic_ompl_pub_.publish(msg_diag_ompl);
    }

    // convert into vector of pose2D
    ROS_DEBUG("Converting Path from ompl PathGeometric format to vector of PoseStamped");
    std::vector<geometry_msgs::Pose> temp_plan_Pose3D;
    geometry_msgs::Pose temp_pose;
    int num_frames_inpath = (int) ompl_path.getStates().size();

    for(int i = 0; i < num_frames_inpath; i++)
    {
        // get frame and tranform it to Pose2D
        OMPLStateSE3ToROSPose3D(ompl_path.getState(i), temp_pose);

        // output states for Debug
        ROS_DEBUG("Coordinates of %dth frame: (x, y, theta) = (%f, %f, %f).", i, temp_pose.position.x, temp_pose.position.y, tf::getYaw(temp_pose.orientation));

        // and append them to plan
        temp_plan_Pose3D.push_back(temp_pose);
    }

    if(interpolate_path_)//TODO
    {
        ROS_DEBUG("Interpolating path to increase density of frames for local planning");
        // interpolate between frames to meet density requirement of local_planner
        bool ipo_success = interpolatePathPose3D(temp_plan_Pose3D);
        if(!ipo_success)
        {
            ROS_ERROR("Something went wrong during interpolation. Probably plan empty. Aborting!");
            return false;
        }
        num_frames_inpath = (int) temp_plan_Pose3D.size();
        ROS_DEBUG("Interpolated Path has %d frames", num_frames_inpath);
    }

    // convert into vector of PoseStamped
    std::vector<geometry_msgs::PoseStamped> temp_plan;
    geometry_msgs::PoseStamped temp_pose_stamped;

    for(int i = 0; i < num_frames_inpath; i++)
    {
        // set Frame to PoseStamped
        ros::Time plan_time = ros::Time::now();

        // set header
        temp_pose_stamped.header.stamp = plan_time;
        temp_pose_stamped.header.frame_id = goal.header.frame_id;//costmap_ros_->getGlobalFrameID();

        // convert Pose2D to pose and set to Pose of PoseStamped
        //		Pose2DToPose(temp_pose_stamped.pose, temp_plan_Pose2D[i]);//GD ,NOT REQUIRED as we are working with pose directly
        temp_pose_stamped.pose = temp_plan_Pose3D[i];

        // append to plan
        temp_plan.push_back(temp_pose_stamped);
    }

    // done -> pass temp_plan to referenced variable plan ...
    ROS_INFO("Global planning finished: Path Found.");
    plan = temp_plan;

    // publish the plan for visualization purposes ...
    publishPlan(plan);


    if(publish_diagnostics_)
    {
        // compose msg with stats
        msg_stats_ompl.path_length = ompl_path.length();
        // set end time for logging of planner statistics
        end_time = ros::Time::now();
        ros::Duration planning_duration = end_time - start_time;
        msg_stats_ompl.total_planning_time = planning_duration.toSec();
        // publish statistics
        stats_ompl_pub_.publish(msg_stats_ompl);
    }

    // and return with true
    return true;
}


//we need to take the footprint of the robot into account when we calculate cost to obstacles
double OMPLPlanner3D::footprintCost2(double x_i, double y_i, double z_i,double dim)
{
    boost::mutex::scoped_lock lock(OctomapMutex);
    geometry_msgs::Point32 p;
    p.x =x_i;
    p.y =y_i;
    p.z =z_i;
    pcd.points.push_back(p);

    if(!tree)
    {
        ROS_INFO("BAD TREE");
        return -1.0;
    }
    octomap::point3d min,max;
//    float dim = 5.0;
    min = octomath::Vector3(x_i-dim,y_i-dim,z_i-dim);
    max = octomath::Vector3(x_i+dim,y_i+dim,z_i+dim);
    for(octomap::OcTree::leaf_bbx_iterator it = tree->begin_leafs_bbx(min,max),
        end=tree->end_leafs_bbx(); it!= end; ++it)
    {
        //manipulate node, e.g.:
        //        std::cout << "Node center: " << it.getCoordinate() << std::endl;
        //        std::cout << "Node size: " << it.getSize() << std::endl;
        //        std::cout << "Node value: " << it->getValue() << std::endl;
        octomap::OcTreeNode* n = tree->search(it.getCoordinate());
        if(n)
        {

            if(tree->isNodeOccupied(n))
            {
//                ROS_INFO("OCCUPANCY: %f",n->getOccupancy());
//                ROS_INFO("NODE IS OCCUPIED");
                //                        pcd.points.push_back(p);
                return 256.0;
            }
        }
    }
    return 0;
}
double OMPLPlanner3D::footprintCost(double x_i, double y_i, double z_i)
{
    boost::mutex::scoped_lock lock(OctomapMutex);
    geometry_msgs::Point32 p;
    p.x =x_i;
    p.y =y_i;
    p.z =z_i;
    pcd.points.push_back(p);

    if(!tree)
    {
        ROS_INFO("BAD TREE");
        return -1.0;
    }
    //    float dim =0.5;
    //    for(float x = x_i - dim ;x < x_i + dim ; x = x+0.1)
    //    {
    //        for(float y = y_i - dim ;y < y_i + dim ; y = y+0.1)
    //        {
    //            for(float z = z_i - dim ;z < z_i + dim ; z = z+0.1)
    //            {
    octomap::OcTreeNode* n = tree->search(x_i,y_i,z_i);
    if(n)
    {
        if(tree->isNodeOccupied(n))
        {
            //                        ROS_INFO("NODE IS OCCUPIED");
            //                        pcd.points.push_back(p);
            return 256.0;
        }
    }
    //            }
    //        }
    //    }

    return 0;
}


bool OMPLPlanner3D::isStateValid3DGrid(const ompl::base::State *state)
{
    geometry_msgs::Pose checked_state;
    double costs = 0.0;

    // transform ompl::base::state back to ros Pose2D
    OMPLStateSE3ToROSPose3D(state, checked_state);

    // check the pose using the footprint_cost check
    ROS_DEBUG("CHECKING NODE at \t%f\t%f\t%f",checked_state.position.x, checked_state.position.y, checked_state.position.z);
    costs = footprintCost2(checked_state.position.x, checked_state.position.y, checked_state.position.z,5.0); //TODO

    if( (costs >= 0) && (costs < max_footprint_cost_) )
    {
        return true;
    }

    return false;
}


bool OMPLPlanner3D::interpolatePathPose3D(std::vector<geometry_msgs::Pose>& path)//TODO
{
    //	std::vector<geometry_msgs::Pose2D> ipoPath;
    //	geometry_msgs::Pose2D last_frame, curr_frame, diff_frame, temp_frame;
    //	double frame_distance, num_insertions;
    //	int path_size = path.size();

    //	// check whether planner is already initialized
    //	if(!initialized_)
    //	{
    //		ROS_ERROR("The planner has not been initialized, please call initialize() to use the planner");
    //		return false;
    //	}

    //	// check whether path is correct - at least 2 Elements
    //	if(path_size < 2)
    //	{
    //		ROS_ERROR("Path is not valid. It has only %d Elements. Interpolation not possible. Aborting.", path_size);
    //		return false;
    //	}

    //	// init plan with start frame
    //	ipoPath.push_back(path[0]);

    //	// make sure plan is dense enough to be processed by local planner
    //	for(int i = 1; i < path_size; i++)
    //	{
    //		// check wether current frame is close enough to last frame --> otherwise insert interpolated frames
    //		last_frame = ipoPath[(ipoPath.size()-1)];
    //		curr_frame = path[i];

    //		// calc distance between frames
    //		diff_frame.x = curr_frame.x - last_frame.x;
    //		diff_frame.y = curr_frame.y - last_frame.y;
    //		diff_frame.theta = curr_frame.theta - last_frame.theta;
    //		// normalize angle
    //		diff_frame.theta = angles::normalize_angle(diff_frame.theta);
    //		// calulate distance --> following is kind of a heuristic measure, ...
    //		// ... as it only takes into account the euclidean distance in the cartesian coordinates
    //		frame_distance = sqrt( diff_frame.x*diff_frame.x + diff_frame.y*diff_frame.y );

    //		// insert frames until path is dense enough
    //		if(frame_distance > max_dist_between_pathframes_)
    //		{
    //			// just in case --> insert one frame more than neccesarry
    //			num_insertions = ceil(frame_distance/max_dist_between_pathframes_);
    //			//ROS_DEBUG("Distance between frames too large (%fm): Inserting %f frames.", frame_distance, num_insertions);
    //			// n insertions create n+1 intervalls --> add one to division
    //			diff_frame.x = diff_frame.x/(num_insertions + 1.0);
    //			diff_frame.y = diff_frame.y/(num_insertions + 1.0);
    //			diff_frame.theta = diff_frame.theta/(num_insertions + 1.0);
    //			for(int j = 1; j <= (int)num_insertions; j++)
    //			{
    //				temp_frame.x = last_frame.x + j*diff_frame.x;
    //				temp_frame.y = last_frame.y + j*diff_frame.y;
    //				temp_frame.theta = last_frame.theta + j*diff_frame.theta;
    //				// normalize angle
    //				temp_frame.theta = angles::normalize_angle(temp_frame.theta);

    //				// append frame to interpolated path
    //				ipoPath.push_back(temp_frame);
    //			}
    //		}

    //		// finally insert frame from path
    //		ipoPath.push_back(curr_frame);
    //	}

    //	// done --> copy ipoPath to refernce-variable and return with true
    //	path = ipoPath;

    return true;
}


// Configuration

void OMPLPlanner3D::setPlannerType(ompl::geometric::SimpleSetup& simple_setup)
{
    // set default planner
    //    std::string default_planner("LBKPIECE");
    std::string default_planner("RRTstar");

    // read desired planner from parameter server
    private_nh_.param("global_planner_type", planner_type_, default_planner);

    ROS_INFO_ONCE("Planner Type = %s",planner_type_.c_str());

    // get SpaceInformationPointer from simple_setup (initialized in makePlan routine)
    ompl::base::SpaceInformationPtr si_ptr = simple_setup.getSpaceInformation();

    // init according planner
    if(planner_type_.compare("EST") == 0)
    {
        // init desired Planner with SpaceInformationPointer
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::EST(si_ptr));
        // set desired planner to simple_setup
        simple_setup.setPlanner(taregt_planner_ptr);
        // done
        return;
    }

    if(planner_type_.compare("KPIECE") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::KPIECE1(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("LBKPIECE") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::LBKPIECE1(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("PRM") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::PRM(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("LazyRRT") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::LazyRRT(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("pRRT") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::pRRT(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("RRT") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::RRT(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("RRTConnect") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::RRTConnect(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("pSBL") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::pSBL(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("SBL") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::SBL(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    if(planner_type_.compare("RRTstar") == 0)
    {
        ompl::base::PlannerPtr taregt_planner_ptr(new ompl::geometric::RRTstar(si_ptr));
        simple_setup.setPlanner(taregt_planner_ptr);
        return;
    }

    // if no string fitted in --> do nothing: by default SimpleSetup will use LBKPIECE1
    return;
}


// Visualization

void OMPLPlanner3D::publishPlan(std::vector<geometry_msgs::PoseStamped> path)
{
    // check whether planner is already initialized --> should be the case anyway but better be sure
    if(!initialized_)
    {
        ROS_ERROR("This planner has not been initialized yet, but it is being used, please call initialize() before use");
        return;
    }

    // check whether there really is a path --> given an empty path we won't do anything
    if(path.empty())
    {
        ROS_INFO("Plan is empty - Nothing to display");
        return;
    }

    // create a message for the plan
    nav_msgs::Path gui_path;
    gui_path.poses.resize(path.size());
    gui_path.header.frame_id = path[0].header.frame_id;
    gui_path.header.stamp = path[0].header.stamp;

    // Extract the plan in world co-ordinates, we assume the path is all in the same frame
    for(unsigned int i=0; i < path.size(); i++)
    {
        gui_path.poses[i] = path[i];
    }

    plan_pub_.publish(gui_path);
}


// Type Conversions

void OMPLPlanner3D::OMPLStateSE3ToROSPose3D(const ompl::base::State* ompl_state, geometry_msgs::Pose& pose)
{
    pose.position.x = ompl_state->as<ompl::base::SE3StateSpace::StateType>()->getX();
    pose.position.y = ompl_state->as<ompl::base::SE3StateSpace::StateType>()->getY();
    pose.position.z = ompl_state->as<ompl::base::SE3StateSpace::StateType>()->getZ();
    tf::quaternionTFToMsg( tf::createQuaternionFromYaw(0.0),pose.orientation );
    const ompl::base::SO3StateSpace::StateType* rot;
    rot = &(ompl_state->as< ompl::base::SE3StateSpace::StateType >()->rotation());
    pose.orientation.w = rot->w;
    pose.orientation.x = rot->x;
    pose.orientation.y = rot->y;
    pose.orientation.z = rot->z;

    return;
}


void OMPLPlanner3D::ROSPose3DToOMPLScopedStateSE3(ompl::base::ScopedState<>& scoped_state,
                                                  const geometry_msgs::Pose pose3D)
{
    // get frame and tranform it to Pose3D
    // access element "->"
    // and cast to SE3 StateType "as<>()" with type "ompl::base::SE3StateSpace::StateType"
    // access member function of actual state "->"
    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->setX(pose3D.position.x);
    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->setY(pose3D.position.y);
    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->setZ(pose3D.position.z);
    //    ompl::base::SO3StateSpace::StateType* rot = scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation();

    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation().setIdentity();
    //    tf::Quaternion q = tf::createQuaternionFromYaw(pose2D.theta);
    //    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation().w = q.w();
    //    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation().x = q.x();
    //    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation().y = q.y();
    //    scoped_state->as<ompl::base::SE3StateSpace::StateType>()->rotation().z = q.z();
    //    scoped_state->as<ompl::base::SO3StateSpace::StateType>()->w = q.w();
    //    scoped_state->as<ompl::base::SO3StateSpace::StateType>()->x = q.x();
    //    scoped_state->as<ompl::base::SO3StateSpace::StateType>()->y = q.y();
    //    scoped_state->as<ompl::base::SO3StateSpace::StateType>()->z = q.z();


    return;
}

//TODO USELESS FUNCTION... IT DOES NOTHING>> copies pose to pose
void OMPLPlanner3D::PoseToPose3D(const geometry_msgs::Pose pose, geometry_msgs::Pose& pose3D)
{
    // use tf-pkg to convert angles
    tf::Pose pose_tf;

    // convert geometry_msgs::PoseStamped to tf::Pose
    tf::poseMsgToTF(pose, pose_tf);

    // now get Euler-Angles from pose_tf
    double useless_pitch, useless_roll, yaw;
    pose_tf.getBasis().getEulerYPR(yaw, useless_pitch, useless_roll);

    // normalize angle
    yaw = angles::normalize_angle(yaw);

    // and set to pose2D
    pose3D.position = pose.position;
    pose3D.orientation = pose.orientation;

    return;
}


void OMPLPlanner3D::Pose2DToPose(geometry_msgs::Pose& pose, const geometry_msgs::Pose2D pose2D)
{
    // use tf-pkg to convert angles
    tf::Quaternion frame_quat;

    // transform angle from euler-angle to quaternion representation
    frame_quat = tf::createQuaternionFromYaw(pose2D.theta);

    // set position
    pose.position.x = pose2D.x;
    pose.position.y = pose2D.y;
    pose.position.z = 0.0;

    // set quaternion
    pose.orientation.x = frame_quat.x();
    pose.orientation.y = frame_quat.y();
    pose.orientation.z = frame_quat.z();
    pose.orientation.w = frame_quat.w();

    return;
}

void OMPLPlanner3D::pathValidator(const ros::TimerEvent&)
{
    sensor_msgs::PointCloud pcd2;
    pcd2.header.frame_id="/nav";

    std::vector<geometry_msgs::PoseStamped> interpPlan_;
    geometry_msgs::PoseStamped start_g,p;
    if(!getCurrentPose(&start_g))
        ROS_ERROR("BAD CURRENT POSE");
    else
        ROS_INFO("CURRENT POSE: %f %f %f",start_g.pose.position.x,start_g.pose.position.y,start_g.pose.position.z);

//    ROS_INFO("_____________%f DISTANCE",distanceToObs(start_g.pose.position.x,start_g.pose.position.y,start_g.pose.position.z));
//    footprintCost2(start_g.pose.position.x,start_g.pose.position.y,start_g.pose.position.z,30.0);
//    ROS_INFO("^^^^^^^^^^^^^");


    if(plan_.size())
    {

        ROS_INFO("Callback 1 triggered");
        int idx;
        double dist=999999.9;
        for(int i=0;i<plan_.size();i++)
        {
            double tempDist = distance3D(start_g.pose,plan_[i].pose);
            if(tempDist < dist)
            {
                idx = i;
                dist = tempDist;
            }
        }

        ROS_INFO("Closest waypoint: %d",idx);
//        idx = 0;
        for(int i=idx;i<plan_.size()-1;i++)
        {
            double dist = distance3D(plan_[i].pose,plan_[i+1].pose);
            double reso = 2.0;
            int div = dist/reso;
            double divX,divY,divZ;
            divX = (plan_[i].pose.position.x - plan_[i+1].pose.position.x)/div;
            divY = (plan_[i].pose.position.y - plan_[i+1].pose.position.y)/div;
            divZ = (plan_[i].pose.position.z - plan_[i+1].pose.position.z)/div;
            //          p.pose.position.x = plan_[i].pose.position.x;
            //          p.pose.position.y = plan_[i].pose.position.y;
            //          p.pose.position.z = plan_[i].pose.position.z;

            //          interpPlan_.push_back(p);
            for(int j=0;j<div;j++)
            {
                p.pose.position.x = plan_[i].pose.position.x - divX*j;
                p.pose.position.y = plan_[i].pose.position.y - divY*j;
                p.pose.position.z = plan_[i].pose.position.z - divZ*j;
                interpPlan_.push_back(p);
            }
        }

        for(int i=0;i<interpPlan_.size();i++)
        {
//            double costs = footprintCost2(interpPlan_[i].pose.position.x, interpPlan_[i].pose.position.y, interpPlan_[i].pose.position.z,5.0);
            double costs = distanceToObs(interpPlan_[i].pose.position.x, interpPlan_[i].pose.position.y, interpPlan_[i].pose.position.z);
            geometry_msgs::Point32 p;
            p.x =interpPlan_[i].pose.position.x;
            p.y =interpPlan_[i].pose.position.y;
            p.z =interpPlan_[i].pose.position.z;
            pcd2.points.push_back(p);

//            ROS_INFO("COST: %f",costs);
            if( (costs >= 2.0) && (costs < max_footprint_cost_) )
            {

            }
            else
            {
                ROS_WARN("REPLANNING");
                makePlan(start_g,goal_,plan_);
            }
        }
    }
    else if(!goalSet)
        ROS_INFO("NO GOAL SET");
    else
    {
        ROS_ERROR("RESENDING GOAL");
        makePlan(start_g,goal_,plan_);
    }

//    if(pcd2.points.size()>0){
//        pcdPub2.publish(pcd2);
//        pcd2.points.clear();
//    }

    if(pcd.points.size()>0){
        pcdPub.publish(pcd);
        pcd.points.clear();
    }
}

double OMPLPlanner3D::distance3D(geometry_msgs::Pose a, geometry_msgs::Pose b)
{
    double dist = (a.position.x - b.position.x)*(a.position.x - b.position.x);
    dist += (a.position.y - b.position.y)*(a.position.y - b.position.y);
    dist += (a.position.z - b.position.z)*(a.position.z - b.position.z);
    dist = sqrt(dist);
    return dist;
}

double OMPLPlanner3D::distanceToObs(double x_i, double y_i, double z_i)
{
    boost::mutex::scoped_lock lock(OctomapMutex);
    sensor_msgs::PointCloud pcd2;
    pcd2.header.frame_id="/nav";

    if(!tree)
    {
        ROS_INFO("GD: BAD TREE");
        return 99999.9;
    }
    octomap::point3d min,max;
    float dim = 5.0;
    float Dist = dim;
    float x,y,z;
    min = octomath::Vector3(x_i-dim,y_i-dim,z_i-dim);
    max = octomath::Vector3(x_i+dim,y_i+dim,z_i+dim);
    for(octomap::OcTree::leaf_bbx_iterator it = tree->begin_leafs_bbx(min,max),
        end=tree->end_leafs_bbx(); it!= end; ++it)
    {
        //manipulate node, e.g.:
//        std::cout << "Node center: " << it.getCoordinate() << std::endl;
//        std::cout << "Node size: " << it.getSize() << std::endl;
//        std::cout << "Node value: " << it->getValue() << std::endl;
        octomap::OcTreeNode* n = tree->search(it.getCoordinate());
        if(n)
        {
            if(tree->isNodeOccupied(n))
            {
                //                        ROS_INFO("GD: NODE IS OCCUPIED");
                //                        pcd.points.push_back(p);
                x = x_i - it.getCoordinate().x();
                y = y_i - it.getCoordinate().y();
                z = z_i - it.getCoordinate().z();

                geometry_msgs::Point32 p;
                p.x =it.getCoordinate().x();
                p.y =it.getCoordinate().y();
                p.z =it.getCoordinate().z();
                pcd2.points.push_back(p);


                float newDist = sqrt(x*x + y*y + z*z);
                if(newDist < Dist){
                    Dist = newDist;
                }
//                ROS_INFO("NODE: %f %f %f",p.x,p.y,p.z);
//                ROS_INFO("NODE: %f %f %f",it.getCoordinate().z(),it.getCoordinate().y(),it.getCoordinate().z());
//                return p;
            }

        }
    }
    if(pcd2.points.size()>0){
        pcdPub2.publish(pcd2);
        pcd2.points.clear();
    }
    return Dist;
}

}




int main(int argc, char **argv)
{
    ros::init(argc, argv, "PLANNER");
    tf::TransformListener tf(ros::Duration(10));
    ompl_planner_3D::OMPLPlanner3D planner("PLANNER",tf);
    ros::NodeHandle n;


    ros::spin();

    return 0;
}
