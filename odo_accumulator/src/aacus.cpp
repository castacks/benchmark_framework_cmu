#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <ros/subscriber.h>
#include <fstream>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/OctomapBinary.h>
#include <nav_msgs/Path.h>
#include <ca_msgs/Float64Stamped.h>

struct point3D
{
    double x;
    double y;
    double z;
};

std::ofstream fd;
double initT;
bool doOnce = true;
octomap::OcTree* tree;
point3D p;
float orig[2];
float endCoord;
bool record=true;


point3D distance(double x_i, double y_i, double z_i);

void odom_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
    if(record)
    {
        ROS_INFO_ONCE("STARTED RECORDING DATA");

        float x,y,z;
        x = /*endCoord - */(float)msg->pose.pose.position.y /*+ orig[0]*/;
        y = (float)msg->pose.pose.position.x /*+ orig[1]*/;
        z = msg->pose.pose.position.z;
        point3D p2;
//        p2.x = (float)p.y + orig[1];
//        p2.y = endCoord - (float)p.x + orig[0];
//        p2.z = p.z;
//        p = p2;
        fd<<std::fixed<<std::setprecision(5)<<(msg->header.stamp.toSec()-initT)<<",  "
//        <<msg->pose.pose.position.x<<",\t"<<msg->pose.pose.position.y<<",\t"<<msg->pose.pose.position.z<<",\t"
        <<std::fixed<<std::setprecision(5)<<x<<",\t"<<std::fixed<<std::setprecision(5)<<y<<",\t"<<std::setprecision(5)<<z<<",\t"
        <<(float)msg->twist.twist.linear.x<<",\t"<<(float)msg->twist.twist.linear.y<<",\t"<<(float)msg->twist.twist.linear.z<<",\t"
        <<p.x<<",\t"<<p.y<<",\t"<<p.z<<",\t"
        <<0.0<<std::endl;


        std::cout<<std::fixed<<std::setprecision(5)<<(msg->header.stamp.toSec()-initT)<<",\t"
//        <<msg->pose.pose.position.x<<",\t"<<msg->pose.pose.position.y<<",\t"<<msg->pose.pose.position.z<<",\t"
        <<std::fixed<<std::setprecision(5)<<x<<",\t"<<std::fixed<<std::setprecision(5)<<y<<",\t"<<std::setprecision(5)<<z<<",\t"
        <<(float)msg->twist.twist.linear.x<<",\t"<<(float)msg->twist.twist.linear.y<<",\t"<<(float)msg->twist.twist.linear.z<<",\t"
        <<p.x<<",\t"<<p.y<<",\t"<<p.z<<",\t"
        <<0.0<<std::endl;
    }
}


void obs_process(const ca_msgs::Float64StampedConstPtr& msg)
{
    p.x = msg->data;
    p.y =p.z = 0.0;
}


void plan_subs_cb(const nav_msgs::Path::ConstPtr& msg)
{
    record = true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ODO_ACCUMULATOR");
//    tf::TransformListener tf(ros::Duration(10));
    ros::NodeHandle nh("/odom_accumulator");

    if(argc<2)
    {
        ROS_ERROR("Please provide the required arguments: \nFileName Scene \n(where Scene = U for Urban and S for Simple)");
        return 0;
    }

    std::string file = argv[1];
    char* scene = argv[2];

    file = file+".dat";

    ROS_INFO("Working for %s scene with filename %s",scene,file.c_str());

    if(*scene == 'S')
    {
        orig[0]=orig[1]=0.0;
        endCoord = 511.0;
    }
    else
    {
        orig[0]=484489.6073;//Y is dataset
        orig[1]=3619378.609;//X in dataset
        endCoord = 1246;
    }

    fd.open(file.c_str());

    fd<<"time(sec),       x(m),       y(m),       z(m),   v_x(m/s),   v_y(m/s),   v_z(m/s),   obs_x(m),   obs_y(m),   obs_z(m),  timeCPU(s)"<<std::endl;
    sleep(1);
    initT = ros::Time::now().toSec();
    std::string obs_topic;
    tree= NULL;
    nh.param("obs_topic", obs_topic, std::string("/distance_to_obstacle_publisher/distance_to_obstacle"));
    ros::Subscriber odom_subs = nh.subscribe("/ulbinterface/pose",1,odom_cb);
    ros::Subscriber obs_subs_ = nh.subscribe(obs_topic,1,obs_process);
//    ros::Subscriber plan_subs_ = nh.subscribe("/GD_PLANNER/GD_PLANNER/plan", 1,plan_subs_cb);
    ros::spin();

//    if(!nh.ok())
//    {
//        fd.close();
//        ROS_ERROR("SHUTTING DOWN");
//    }

    return 0;
}
