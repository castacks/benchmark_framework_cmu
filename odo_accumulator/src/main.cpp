#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <ros/subscriber.h>
#include <fstream>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/OctomapBinary.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/PointCloud.h>

struct point3D
{
    double x;
    double y;
    double z;
};

std::ofstream fd;
double initT;
bool doOnce = true;
octomap::OcTree* tree;
point3D p;
sensor_msgs::PointCloud pcd;
float orig[2];
float endCoord;
bool record=true;
bool simple=true;

point3D distance(double x_i, double y_i, double z_i);
point3D distance2(double x_i, double y_i, double z_i);

void odom_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
    if(record)
    {
        ROS_INFO_ONCE("STARTED RECORDING DATA");
        p = distance2(msg->pose.pose.position.x,msg->pose.pose.position.y,msg->pose.pose.position.z);
        float x,y,z;
        if(simple)
        {
            y = (float)msg->pose.pose.position.y + orig[0];
            x = (float)msg->pose.pose.position.x + orig[1];
        }
        else
        {
            x = endCoord - (float)msg->pose.pose.position.y + orig[0];
            y = /*endCoord -*/ (float)msg->pose.pose.position.x + orig[1];
        }
        z = msg->pose.pose.position.z;
        //        point3D p2;
        //        p2.x = endCoord - (float)p.y + orig[0];//(float)p.y + orig[1];
        //        p2.y = (float)p.x + orig[1];//endCoord - (float)p.x + orig[0];
        //        p2.z = p.z;
        //        p = p2;
        fd<<std::fixed<<std::setprecision(5)<<(msg->header.stamp.toSec()-initT)<<",  "
            //        <<msg->pose.pose.position.x<<",\t"<<msg->pose.pose.position.y<<",\t"<<msg->pose.pose.position.z<<",\t"
         <<std::fixed<<std::setprecision(5)<<x<<",\t"<<std::fixed<<std::setprecision(5)<<y<<",\t"<<std::setprecision(5)<<z<<",\t"
        <<(float)msg->twist.twist.linear.x<<",\t"<<(float)msg->twist.twist.linear.y<<",\t"<<(float)msg->twist.twist.linear.z<<",\t"
        <<p.x<<",\t"<<p.y<<",\t"<<p.z<<",\t"
        <<0.0<<std::endl;


        std::cout<<std::fixed<<std::setprecision(5)<<(msg->header.stamp.toSec()-initT)<<",\t"
                   //        <<msg->pose.pose.position.x<<",\t"<<msg->pose.pose.position.y<<",\t"<<msg->pose.pose.position.z<<",\t"
                <<std::fixed<<std::setprecision(5)<<x<<",\t"<<std::fixed<<std::setprecision(5)<<y<<",\t"<<std::setprecision(5)<<z<<",\t"
               <<(float)msg->twist.twist.linear.x<<",\t"<<(float)msg->twist.twist.linear.y<<",\t"<<(float)msg->twist.twist.linear.z<<",\t"
              <<p.x<<",\t"<<p.y<<",\t"<<p.z<<",\t"
             <<0.0<<std::endl;
    }
}


void octomap_process(const octomap_msgs::OctomapBinary::ConstPtr& msg)
{
    //    tree = dynamic_cast<octomap::OcTree*>(octomap_msgs::binaryMsgDataToMap(msg->data));
    ROS_INFO_ONCE("RECVD OCTOMAP");
    delete tree;
    if(tree)
    {
        tree = NULL;
    }
    if(!tree)
    {
        tree = dynamic_cast<octomap::OcTree*>(octomap_msgs::fullMsgDataToMap(msg->data));
    }
    if(tree && doOnce)
    {
        doOnce = false;
        tree->setOccupancyThres(0.5);
        tree->setProbHit(0.7);
        tree->setProbMiss(0.3);
        tree->setClampingThresMax(0.95);
        tree->setClampingThresMin(0.1);

        //        ROS_INFO("TREE MEMORY USAGE: %d",tree->size());
        //        if(tree->size()>20000)
        //            tree->prune();
    }
}

point3D distance(double x_i, double y_i, double z_i)
{
    point3D p;
    p.x = p.y = p.z = -1000.0;
    if(!tree)
    {
        ROS_INFO("GD: BAD TREE");
        return p;
    }
    float dim =5.0;
    //COULD START WITH Zs to explore on horizontal axis first
    for(float x = x_i - dim ;x < x_i + dim ; x = x+0.1)
    {
        for(float y = y_i - dim ;y < y_i + dim ; y = y+0.1)
        {
            for(float z = z_i - dim ;z < z_i + dim ; z = z+0.1)
            {
                octomap::OcTreeNode* n = tree->search(x,y,z);
                if(n)
                {
                    if(tree->isNodeOccupied(n))
                    {
                        //                        ROS_INFO("GD: NODE IS OCCUPIED");
                        //                        double dist = sqrt(x*x + y*y + z*z);
                        //                        return (dist);
                        p.x = x_i - x;
                        p.y = y_i - y;
                        p.z = z_i - z;
                        ROS_INFO("NODE: %f %f %f",p.x,p.y,p.z);
                        return p;
                    }
                }
            }
        }
    }

    return p;
}

point3D distance2(double x_i, double y_i, double z_i)
{
    point3D p;
    p.x = 0.0;
    p.y = 0.0;
    p.z = -1.0;//z_i;

    if(!tree)
    {
        ROS_INFO("GD: BAD TREE");
        return p;
    }
    octomap::point3d min,max;
    float dim = 30.0;
    float Dist = dim;
    min = octomath::Vector3(x_i-dim,y_i-dim,z_i-dim);
    max = octomath::Vector3(x_i+dim,y_i+dim,z_i+dim);
    for(octomap::OcTree::leaf_bbx_iterator it = tree->begin_leafs_bbx(min,max),
        end=tree->end_leafs_bbx(); it!= end; ++it)
    {
        //manipulate node, e.g.:
        //        std::cout << "Node center: " << it.getCoordinate() << std::endl;
        //        std::cout << "Node size: " << it.getSize() << std::endl;
        //        std::cout << "Node value: " << it->getValue() << std::endl;
        octomap::OcTreeNode* n = tree->search(it.getCoordinate());
        if(n)
        {
            if(tree->isNodeOccupied(n))
            {
                //                        ROS_INFO("GD: NODE IS OCCUPIED");
                //                        pcd.points.push_back(p);
                p.x = x_i - it.getCoordinate().x();
                p.y = y_i - it.getCoordinate().y();
                p.z = z_i - it.getCoordinate().z();
                float newDist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
                if(newDist < Dist){
                    Dist = newDist;
                    geometry_msgs::Point32 pcdPoint;
                    pcdPoint.x = it.getCoordinate().x();
                    pcdPoint.y = it.getCoordinate().y();
                    pcdPoint.z = it.getCoordinate().z();
                    pcd.points.push_back(pcdPoint);
                }



                //                ROS_INFO("NODE: %f %f %f",p.x,p.y,p.z);
                //                ROS_INFO("NODE: %f %f %f",it.getCoordinate().z(),it.getCoordinate().y(),it.getCoordinate().z());
                //                return p;
            }
        }
    }
    geometry_msgs::Point32 pcdPoint;
    pcdPoint.x = x_i;
    pcdPoint.y = y_i;
    pcdPoint.z = z_i;
    pcd.points.push_back(pcdPoint);

    //setting p to distance metric than postion on obstacle;
    p.x =p.y =0.0;
    p.z = Dist;
    return p;
}

void plan_subs_cb(const nav_msgs::Path::ConstPtr& msg)
{
    record = true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ODO_ACCUMULATOR");
    //    tf::TransformListener tf(ros::Duration(10));
    ros::NodeHandle nh("/odom_accumulator");

    if(argc<2)
    {
        ROS_ERROR("Please provide the required arguments: \nFileName Scene \n(where Scene = U for Urban and S for Simple)");
        return 0;
    }

    std::string file = argv[1];
    char* scene = argv[2];

    file = file+".dat";

    ROS_INFO("Working for %s scene with filename %s",scene,file.c_str());

    if(*scene == 'S')
    {
        orig[0]= -100.0;
        orig[1]= -50.0;
        endCoord = 0;
        simple = true;
    }
    else
    {
        orig[0]=484489.6073;//Y is dataset
        orig[1]=3619378.609;//X in dataset
        endCoord = 1246;
        simple = false;
    }

    fd.open(file.c_str());

    fd<<"time(sec),       x(m),       y(m),       z(m),   v_x(m/s),   v_y(m/s),   v_z(m/s),   obs_x(m),   obs_y(m),   obs_z(m),  timeCPU(s)"<<std::endl;
    sleep(1);
    initT = ros::Time::now().toSec();
    std::string octo_topic;
    tree= NULL;
    nh.param("octomap_topic", octo_topic, std::string("/octomap_full"));
    ros::Subscriber odom_subs = nh.subscribe("/ground_truth/state",1,odom_cb);
    ros::Subscriber octomap_subs_ = nh.subscribe(octo_topic,1,octomap_process);
    ros::Subscriber plan_subs_ = nh.subscribe("/GD_PLANNER/GD_PLANNER/plan", 1,plan_subs_cb);
    ros::spin();

    //    if(!nh.ok())
    //    {
    //        fd.close();
    //        ROS_ERROR("SHUTTING DOWN");
    //    }

    return 0;
}
