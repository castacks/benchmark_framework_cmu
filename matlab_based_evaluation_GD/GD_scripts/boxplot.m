% show a boxplot
function boxplot(inQuartiles)
   tiny = inQuartiles(5)*0.003;
   line( [inQuartiles(1), inQuartiles(5)], [0,0], 'LineWidth', 3);
   hold all; line( [inQuartiles(2), inQuartiles(4)], [0,0], 'LineWidth', 8);
   hold all; line( [inQuartiles(3)-tiny, inQuartiles(3)+tiny], [0,0], 'LineWidth', 16);
   if (sum(inQuartiles.^2)) > 0
      xlim([0 max(inQuartiles)]);
   end
end