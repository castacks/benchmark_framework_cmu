figure(1);
hleg1 = legend('baseline','PLANNER','Location','Best');
set(hleg1, 'Box', 'off')

figure(2);
set(2,'Position',[1 1 800 600]);
hleg1 = legend('baseline','baseline 95%ile','PLANNER','PLANNER 95%ile','Location','Best');
set(hleg1, 'Box', 'off')

figure(3);
hleg1 = legend('baseline','PLANNER','Location','Best');
set(hleg1, 'Box', 'off')

figure(4);
set(4,'Position',[1 1 800 600]);
hleg1 = legend('baseline','baseline 95%ile','baseline 5%ile','PLANNER','PLANNER 95%ile','PLANNER 5%ile','Location','Best');
set(hleg1, 'Box', 'off')

%SAVING
figure(1);
hgexport(gcf, ['../Results/' header 'M'], hgexport('factorystyle'), 'Format', 'pdf');
figure(2);
hgexport(gcf, ['../Results/' header 'C'], hgexport('factorystyle'), 'Format', 'pdf');
figure(3);
hgexport(gcf, ['../Results/' header 'T'], hgexport('factorystyle'), 'Format', 'pdf');
figure(4);
hgexport(gcf, ['../Results/' header 'H'], hgexport('factorystyle'), 'Format', 'pdf');


%%% DONOT USE THE FOLLOWING %%%
% set(hleg1, 'Color', 'none')
% set(hleg1, 'Position', [.1,.2,.1,.2]);

 
% saveas(1,['Results/' header 'M'],'png'); %name is a string
% saveas(2,['Results/' header 'C'],'png'); %name is a string
% saveas(3,['Results/' header 'T'],'png'); %name is a string
% saveas(4,['Results/' header 'H'],'tif'); %name is a string
% print(4,'-dps',['Results/' header 'M']);