%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parse a matrix
function outMatrix = matrixScan(fid)
    outMatrix = [];
    while 1
        tline = fgets(fid);
        if ~ischar(tline)
            break
        end
        newRow = sscanf(tline, '%f,', [1,inf]);
        if ( size(newRow)>0 )
            outMatrix = [outMatrix; newRow];
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%