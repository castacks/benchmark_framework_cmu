% function trajMetricsUMN9(infilename)
% infilename = '/home/aeroscout/geetesh/ros/odo_accumulator/data.dat';
% infilename = 'cube.dat';
%
% Purpose: to compute and output trajectory metrics for comparing benchmarks
% Inputs:
%   infilename: name of the input file (.dat format; comma-delimited text)
% Created: Chad Goerzen
% Modified:
%   2010-02-22
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% open the file
fid = fopen(infilename);

% ignore the first line
%    assume first line format:
%       time(s),        x,y,z (m),      v_x,v_y,v_z (m/s),      obs_x,obs_y,obs_z (m)
headingLine = fgetl(fid);

% parse the data array
data = matrixScan(fid);
data = data/factor;
data(:,1) = data(:,1) * factor;
szData = size(data);

% close the file
fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run parameters
cutoff = 1;      % proportion of total bandwidth
maxAngRate = 90; % deg/s
rotorRadius = 1.5; % m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extract variables
% time
time = data(:,1)';
  %diffTime = [diff(time) (max(time)-min(time))/szData(1)]
  diffTime = diff(time);
  numSamples = max(size(time));
  timeDst = time;


% position
xP = data(:,2)';
yP = data(:,3)';
zP = data(:,4)';

% velocity
xV = data(:,5)';
yV = data(:,6)';
zV = data(:,7)';
magV = sqrt(xV.^2 + yV.^2 + zV.^2);

% acceleration
xA = diff(xV)./diffTime;                                              % m/s/s
yA = diff(yV)./diffTime;
zA = diff(zV)./diffTime;

magA = sqrt(xA.^2 + yA.^2 + zA.^2);
magA = [magA 0]; % pad with a 0 for plotting purposes


% horizontal power
horizV = sqrt(xV.^2 + yV.^2);
powerHoriz = (703.95*sqrt(9.8^2+magA.^2).*sqrt(sqrt((horizV./7.41).^4/4+1)-(horizV./7.41).^2/2)+0.18*horizV.^3+1669.40+0.11*horizV.^2)*(1+1/(89.25*1.775))/0.91;
% vertical power
vInduced = 7.41; % induced velocity (m/s)
powerInduced = 703.95*sqrt(9.8^2+magA.^2); % induced power (W)
  % hovering (zV == 0)
  powerVert = powerInduced.*(zV == 0);
  % descending (zV < 0)
  powerVert = powerVert + powerInduced.*(zV<0).*(1+zV./vInduced);
  % climbing (zV > 0)
  powerVert = powerVert + powerInduced.*(zV>0).*(zV./(2*vInduced)+sqrt((zV./(2*vInduced)).^2+1));



% heading
hdgPsi = unwrap(atan2(yV,xV));
hdgPsiRate = diff(hdgPsi)./diffTime;
%hdgPsiRate = [hdgPsiRate 0];

% glide slope
hdgGam = acos(zV./max(magV,1e-1));
hdgGamRate = diff(hdgGam)./diffTime;
%hdgGamRate = [hdgGamRate 0];

% combined heading rate (haversine great circle distance formula)
angRate = 2*asin( sqrt( sin(hdgPsiRate/2).^2 + 1*cos(hdgPsiRate).*sin(hdgGamRate).^2 ) );
  %apply +-  maximum rate
  angRate = min(angRate, maxAngRate*pi/180);
  angRate = max(angRate, -maxAngRate*pi/180);

if (szData(2) > 9)
    % obstacle location
    xObsDst = data(:,8)';
    yObsDst = data(:,9)';
    zObsDst = data(:,10)';
    % check for '0' entries, and calculate distance
%     if (sum(xObsDst.^2) > 0) xObsDst = xObsDst - xP; end
%     if (sum(yObsDst.^2) > 0) yObsDst = yObsDst - yP; end
%     if (sum(zObsDst.^2) > 0) zObsDst = zObsDst - zP; end
    obsDst = sqrt( xObsDst.^2 +  yObsDst.^2 +  zObsDst.^2 ) - rotorRadius;
%     obsDst = max(obsDst,0);
%     obsDst = min(obsDst, zP);
else
    xObsDst = zeros(szData(1),1);
    yObsDst = zeros(szData(1),1);
    zObsDst = zeros(szData(1),1);
    obsDst = zeros(szData(1),1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set up figure positions
ScreenSize = get(0,'ScreenSize');

% pos1 = [ 1, 0.5*ScreenSize(4),                 0.3*ScreenSize(3), 0.9*ScreenSize(4)];
% pos2 = [ 1, 1,                                 1.0*ScreenSize(3), 0.4*ScreenSize(4)];
% pos3 = [  .5*ScreenSize(3), 0.5*ScreenSize(4), 0.5*ScreenSize(3), 0.4*ScreenSize(4)];
pos1 = [ 1, 0.5*ScreenSize(4),                 0.15*ScreenSize(3), 1*ScreenSize(4)];
pos2 = [ 1, 1,                                 0.7*ScreenSize(3), 0.2*ScreenSize(4)];
pos3 = [  .5*ScreenSize(3), 0.5*ScreenSize(4), 0.25*ScreenSize(3), 0.2*ScreenSize(4)];
figure(1); set(1, 'Position', pos1);% set(f1,'DefaultAxesLineStyleOrder','-|--|:|-.');
figure(2);% set(2, 'Position', pos2);% set(f2,'DefaultAxesLineStyleOrder','-|--|:|-.');
figure(3);% set(3, 'Position', pos3);% set(f3,'DefaultAxesLineStyleOrder','-|--|:|-.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% output results
trajTime = max(time)-min(time);                             % trajectory time (s)
accelMagAvg = sum(magA.*[diffTime 0])/trajTime;                 % average acceleration magnitude (m/s/s)
angRateAvg = 180/pi*sum(abs(angRate.*diffTime))/trajTime;   % average heading rate (deg/s)
energy = sum( (powerHoriz+powerVert).*[diffTime 0])/trajTime;% total energy (J)
%obsDstQ = quartiles(obsDst);                                % obstacle distance (m)
display(sprintf( '  total trajectory time (s): %f   ', trajTime ));
display(sprintf( '  average acceleration magnitude (m/s/s): %f   ', accelMagAvg ));
display(sprintf( '  average heading rate (deg/s): %f   ', angRateAvg ));
display(sprintf( '  total energy (J): %f   ', energy ));
%display(sprintf( '  obstacle distance quartiles (m):' )); disp(   obsDstQ );

% non-output metrics
deltaS = sqrt(diff(xP).^2 + diff(yP).^2 + diff(zP).^2);     % path segment length
trajLength = sum(deltaS);                                   % trajectory length
vAvg = trajLength./trajTime;                                % average speed
obsDstAvg = mean(obsDst);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% show plots
% normalize start time to 0
timeOut = time - time(1);
timeOutDst = timeDst - timeDst(1);

figure(1);

  hold all; subplot(411); plot(timeOut, magV,'color',color,'LineWidth', 2); title('Speed'); xlabel('time (s)'); ylabel('speed (m/s)');
  hold all; subplot(412); plot(timeOut, magA,'color',color,'LineWidth', 2); title('Acceleration'); xlabel('time (s)'); ylabel('acceleration (m/s/s)');
  hold all; subplot(413); plot(timeOut, [angRate*180/pi 0],'color',color,'LineWidth', 2); title('Angular rate'); xlabel('time (s)'); ylabel('rate (deg/s)');
  hold all; subplot(414); plot(timeOutDst, obsDst,'color',color,'LineWidth', 2); title('Obstacle distance'); xlabel('time (s)'); ylabel('distance (m)');
%                           ylim('auto'); ylim( ylim.*[0 1] );
%   hold all; subplot(515); plot(timeOut, powerHoriz + powerVert,'color',color); title('Power consumption'); xlabel('time (s)'); ylabel('power (W)');
%   subplot(224); hold all;  plot(timeOut, powerHoriz, 'LineWidth', 2); title('Power use'); xlabel('time (s)'); ylabel('power (W)');
%              hold all;     plot(timeOut, powerVert, 'LineWidth', 2); title('Power use'); xlabel('time (s)'); ylabel('power (W)');
%              hold all;     plot(timeOut, powerHoriz + powerVert, 'LineWidth', 2); title('Power use'); xlabel('time (s)'); ylabel('power (W)');
%              %legend('Horizontal power', 'Vertical power', 'Total power', 'Location', 'EastOutside'); ylim( ylim.*[0 1] );



  % generate histogram data
  sFact = 1;
  histRes = sFact*sqrt(numSamples);
  [magVHist, magVHistP] = hist(magV,histRes);
  [magAHist, magAHistP] = hist(magA,histRes);
  [angRateHist, angRateHistP] = hist(angRate*180/pi,histRes);
  [obsDstHist, obsDstHistP] = hist(obsDst,histRes);
  % generate CDF data
  [magVHistP, magVCdf] = CDF(magV,histRes);
  [magAHistP, magACdf] = CDF(magA,histRes);
  [angRateHistP, angRateCdf] = CDF(angRate*180/pi,histRes);
  [obsDstHistP, obsDstCdf] = CDF(obsDst,histRes);
  [powerHistP, powerCdf] = CDF(powerHoriz+powerVert,histRes);
  
  % plot histograms
  figure(4);
  subplot(221); %bar(magVHistP, magVHist/max(size(magV)), 'BarWidth', 1, 'EdgeColor', 'none', 'FaceColor', [.5 .5 .5]);
                plot(magVHistP, magVHist/max(size(magV)),'color',color,'LineWidth', 2);
                hold all; title('Velocity histogram'); xlabel('velocity (m/s)'); ylabel('n'); %xlim('auto'); xlim(xlim.*[0,1]);
  subplot(222); %bar(magAHistP, magAHist/max(size(magA)), 'BarWidth', 1, 'EdgeColor', 'none', 'FaceColor', [.5 .5 .5]);
                plot(magAHistP, magAHist/max(size(magA)),'color',color,'LineWidth', 2);
                hold all; title('Acceleration histogram'); xlabel('acceleration (m/s/s)'); ylabel('n'); %xlim('auto'); xlim(xlim.*[0,1]);
  subplot(223); %bar(angRateHistP, angRateHist/max(size(angRate)), 'BarWidth', 1, 'EdgeColor', 'none', 'FaceColor', [.5 .5 .5] );
                plot(angRateHistP, angRateHist/max(size(angRate)),'color',color,'LineWidth', 2);
                hold all; title('Angular rate histogram'); xlabel('angular rate (deg/s)'); ylabel('n'); %xlim('auto'); xlim(xlim.*[0,1]);
  subplot(224); %bar(obsDstHistP, obsDstHist/max(size(obsDst)), 'BarWidth', 1, 'EdgeColor', 'none', 'FaceColor', [.5 .5 .5]);
                plot(obsDstHistP, obsDstHist/max(size(obsDst)),'color',color,'LineWidth', 2);
                hold all; title('Obstacle distance histogram'); xlabel('distance (m)'); ylabel('n'); %xlim('auto'); xlim(xlim.*[0,1]);
                
 %plot percentiles
  subplot(221);i = calcPerc(magV,histRes,0.95);a=[magVHistP(i(1)) magVHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(222);i = calcPerc(magA,histRes,0.95);a=[magAHistP(i(1)) magAHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(223);i = calcPerc(angRate,histRes,0.95);a=[angRateHistP(i(1)) angRateHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(224);i = calcPerc(obsDst,histRes,0.95);a=[obsDstHistP(i(1)) obsDstHistP(i(1))];plot(a,[0 1],'--','color',color);
  
  subplot(221);i = calcPerc(magV,histRes,0.05);a=[magVHistP(i(1)) magVHistP(i(1))];plot(a,[0 1],'-.','color',color);
  subplot(222);i = calcPerc(magA,histRes,0.05);a=[magAHistP(i(1)) magAHistP(i(1))];plot(a,[0 1],'-.','color',color);
  subplot(223);i = calcPerc(angRate,histRes,0.05);a=[angRateHistP(i(1)) angRateHistP(i(1))];plot(a,[0 1],'-.','color',color);
  subplot(224);i = calcPerc(obsDst,histRes,0.05);a=[obsDstHistP(i(1)) obsDstHistP(i(1))];plot(a,[0 1],'-.','color',color);
  
  

  % plot CDF
  figure(2)
  subplot(221); plot(magVHistP, magVCdf,'color',color,'LineWidth', 2);
                hold all; title('Speed CDF'); xlabel('velocity (m/s)'); ylabel('phi'); %xlim('auto'); xlim(xlim.*[0,1]); ylim([0,1]);
  subplot(222); plot(magAHistP, magACdf,'color',color,'LineWidth', 2);
                hold all; title('Acceleration CDF'); xlabel('acceleration (m/s/s)'); ylabel('phi'); %xlim('auto'); xlim(xlim.*[0,1]); ylim([0,1]);
  subplot(223); plot(angRateHistP, angRateCdf ,'color',color,'LineWidth', 2);
                hold all; title('Angular rate CDF'); xlabel('angular rate (deg/s)'); ylabel('phi'); %xlim('auto'); xlim(xlim.*[0,1]); ylim([0,1]);
  subplot(224); plot(obsDstHistP, obsDstCdf,'color',color,'LineWidth', 2);
                hold all; title('Obstacle distance CDF'); xlabel('distance (m)'); ylabel('phi'); %xlim('auto');% xlim(xlim.*[0,1]); ylim([0,1]);
%   subplot(155); plot(powerHistP, powerCdf,'color',color);
%                 hold all; title('Power CDF'); xlabel('power (W)'); ylabel('phi'); xlim('auto');  ylim([0,1]);

 %plot percentiles
  subplot(221);i = calcPerc(magV,histRes,0.95);a=[magVHistP(i(1)) magVHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(222);i = calcPerc(magA,histRes,0.95);a=[magAHistP(i(1)) magAHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(223);i = calcPerc(angRate,histRes,0.95);a=[angRateHistP(i(1)) angRateHistP(i(1))];plot(a,[0 1],'--','color',color);
  subplot(224);i = calcPerc(obsDst,histRes,0.95);a=[obsDstHistP(i(1)) obsDstHistP(i(1))];plot(a,[0 1],'--','color',color);
  
  
  
figure(3);
  %optional: rotate and start trajectory at 0,0,0
  xPout = (xP); yPout = (yP); zPout = zP;      % original (no translation)
  %xPout = (xP - xP(1)); yPout = (yP - yP(1)); zPout = zP;      % natural
  %yPout = -(xP - xP(1)); xPout = (yP - yP(1)); zPout = zP;      % rotate 90 deg
 %yPout = (xP - xP(1)); xPout = -(yP - yP(1)); zPout = zP;      % rotate 90 deg and mirror
  %yPout = -(xP - xP(1)); xPout = -(yP - yP(1)); zPout = zP;    % rotate -90 deg and mirror
  %xPout = -(xP - xP(1)); yPout = -(yP - yP(1)); zPout = zP;    % mirror
  hold all;   plot3(xPout,yPout,zPout, 'LineWidth', 2,'color',color); axis equal; title('Trajectory'); xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)');
                view( [0 90] );






             
% end
