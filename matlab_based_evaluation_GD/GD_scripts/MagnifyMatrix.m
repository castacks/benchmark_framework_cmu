function X1 = MagnifyMatrix(X, Factor)

[h w ] = size(X);

XRes = [];
for i = 1 : w
     XTmp = [];
     for j = 1 : h
         XTmp = [XTmp ; repmat( X(j,i) * Factor, Factor , Factor )];
     end
     XRes = [ XRes , XTmp ];
end
X1 = XRes;
end