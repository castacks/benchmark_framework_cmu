x11 = datar(2);  % Two meters east of the upper left corner
y11 = datar(((datar(1)+2)+(datar((datar(1)+2)))));  % Two meters south of the upper left corner
dx =  cellSize;
dy = -cellSize;
refmat = makerefmat(x11, y11, dx, dy)

R = refmatToMapRasterReference(refmat, size(data));
R.RasterInterpretation = 'postings';
key.GTModelTypeGeoKey  = 1;  % Projected Coordinate System (PCS)
key.GTRasterTypeGeoKey = 2;  % PixelIsPoint
key.ProjectedCSTypeGeoKey = 26719;
%filename = 'bm6_CubeBaffle.tif';
geotiffwrite(filename, data, R, 'GeoKeyDirectoryTag', key);
%clc;clear;