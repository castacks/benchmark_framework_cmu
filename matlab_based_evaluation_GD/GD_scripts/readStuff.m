% clc;clear;
filename = 'SanDiegoDowntown2005';
if(strcmp(filename,'SanDiegoDowntown2005'))
    URBAN = 1;
    cellSize = 2.44;
else
    URBAN = 0;    
    cellSize = 1;
end
fileToRead1 = [filename '.hmap'];
%IMPORTFILE(FILETOREAD1)
%  Imports data from the specified file
%  FILETOREAD1:  file to read

%  Auto-generated by MATLAB on 08-Feb-2014 22:47:00

% Import the file
rawData1 = importdata(fileToRead1);

% For some simple files (such as a CSV or JPEG files), IMPORTDATA might
% return a simple array.  If so, generate a structure so that the output
% matches that from the Import Wizard.
[~,name] = fileparts(fileToRead1);
newData1.(genvarname(name)) = rawData1;

% Create new variables in the base workspace from those fields.
vars = fieldnames(newData1);
for i = 1:length(vars)
    assignin('base', vars{i}, newData1.(vars{i}));
end
datar =rawData1;
% xy = datar(1:404);
X = datar(2:(datar(1)+1));
Y = datar((datar(1)+3):((datar(1)+2)+(datar((datar(1)+2)))));


DELIMITER = ',';
HEADERLINES = ((datar(1)+2)+(datar((datar(1)+2))));

% Import the file
newData1 = importdata(fileToRead1, DELIMITER, HEADERLINES);

% Create new variables in the base workspace from those fields.
vars = fieldnames(newData1);
for i = 1:length(vars)
    assignin('base', vars{i}, newData1.(vars{i}));
end
if(URBAN)
    data = newData1.data';
    data=flipud(data);
    sprintf('URBAN')
else
    data = newData1.data;
    sprintf('SIMPLE')
end
data = MagnifyMatrix(data,10);

geotiffGD;

start = [x11+500 y11-1000];
goal = [x11+1500 y11-1000];