function [ p ] = calcPerc( mag , histRes,perc)
%CDF Summary of this function goes here
%   Detailed explanation goes here
[magHist, magHistP] = hist(mag,histRes);
cdf_var=[];
cdf_var(1) = magHist(1);
for i=2:size(magHist,2)
    cdf_var(i) = cdf_var(i-1) + magHist(i);
end
v = int64(sum(magHist)*perc);
p = find(cdf_var >= v);
end

