function [ p ,  c] = CDF2( mag , histRes)
%CDF Summary of this function goes here
%   Detailed explanation goes here
[magHist, magHistP] = hist(mag,histRes);
p = magHistP;
c=cdf('Normal',magHistP,mean(mag),var(mag,1));
% cdf_var(1) = magHist(1);
% for i=2:size(magHist,2)
%     cdf_var(i) = cdf_var(i-1) + magHist(i);
% end
% p = magHistP;
% c = cdf_var./sum(magHist);
end

