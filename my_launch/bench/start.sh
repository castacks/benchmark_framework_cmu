#!/bin/bash
export startX=50   #50.5 #519.3909999998
export startY=100   #100.5 #831.6073
export startZ=10    #10.0 #28.0

#ln -sf world/bm2_PointObstacle.world model.world;
#ln -sf world/bm3_Wall.world model.world;
ln -sf world/bm4_Cube.world model.world;
#ln -sf world/bm5_WallBaffle.world model.world;
#ln -sf world/bm6_CubeBaffle.world model.world;
#ln -sf world/SDDowntown.world model.world;

roslaunch my_launch bench_all.launch &

#rosrun ompl_planner3D plan_on_map & #CHECK IF IT'S BEING INVOKED IN bench_all.launch

#sending Goal
sleep 10
rostopic pub -1 move_base_simple/goal geometry_msgs/PoseStamped  '{header: {frame_id: /nav}, pose: {position:  {x: 150.0, y: 100.0, z: 10.0}, orientation: {x: 0.0,y: 0.0,z: 0.0,w: 1.0}}}' &

#SIMPLE:
#   X               Y           Z   Location
#____________________________________________
#   50	            100	        10	start
#   150	            100	        10	target	


#SAN DEIGO/URBAN: 
#   X               Y           Z   Location
#____________________________________________
#   638.3909999998	660.6073	39	target	A
#   519.3909999998	831.6073	28	A1	
#   693.3909999998	853.6073	30	A2	
#   808.3909999998	747.6073	44	A3	
#   804.3909999998	573.6073	55	A4	
#   737.3909999998	482.6073	50	A5	
#   593.3909999998	461.6073	35	A6	
#   1195.3909999998	691.6073	52	target	B
#   1169.3909999998	483.6073	67	B1	
#   1051.3909999998	572.6073	66	B2	
#   1052.3909999998	832.6073	47	B3	
#   1173.3909999998	887.6073	43	B4	
